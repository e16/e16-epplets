#ifndef COMMS_H
#define COMMS_H 1

#include <X11/Xlib.h>

extern Display *disp;
extern Window   root;

void            CommsSetup(void);
int             CommsSend(const char *buf, unsigned int len);
char           *CommsWaitForMessage(void);
void            CommsHandleDestroy(Window xwin);
int             CommsHandlePropertyNotify(XEvent * ev);
char           *CommsHandleClientMessage(XEvent * ev);

#endif                          /* COMMS_H */
